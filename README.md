## Autor: Vinícios Faustino de Carvalho
# O desafio:
O desafio proposto pode ser encontrado através do [link](https://www.notion.so/Desafio-t-cnico-2f53347cf16a418fb75c67c978ab1a0e)
# Módulos:
Para a resolução do problema, optei por dividir o código em diferentes módulos para agrupar responsabilidades e funcionalidades.
## Avaliação:
Este módulo tem por objetivo manter o contrato de como deve funcionar uma avaliação para atribuir a pontuação para cada família.
No problema dado, eram apenas 3 critérios a serem pontuados, porém, como indicado, caso hajam novos critérios, eles podem ser facilmente implementados nesse módulo, sem necessidade de alteração no código já existente.

## Dados:
A ideia desse módulo é que ele seja responsável por carregar os dados do banco para as devidas classes. Neste caso, ele apenas converte uma string no formato Json para as classes pertinentes, mas sua independência do restante do código permite que as regras do negócio não seja alteradas caso haja uma mudança na forma como os dados são obtidos.

## Modelos:
A modelagem das classes que representam entidades do mundo real foram deixadas nesse módulo para, evitando que se misturassem com a lógica e as regras do negócio, além de facilitar implementação de novos atributos para cada entidade.

O *CadastroFamilia* representa o cadastro da familia no programa de casas, contendo as informações inerentes.

*Conjuge*, *Dependente* e *Pretendente* são classes que extendem de *Pessoa* para permitir que novos atributos possam ser designados para cada um de maneira independente.

*Familia* é a modelagem para os dados de entrada sobre a família.

*FamiliaContemplada* é uma classe que possui as informações que devem ser passadas para o módulo *Contemplados*, não possuindo por sua vez as informações completas da família, apenas seu id, pontuações e data de seleção.

Ter uma classe *Renda* independente permite que novas informações possam ser adicionadas à classe sem comprometer o funcionamento de outras funcionalidades e facilitando a manutenção.

## Validação
O módulo de validação tem por objetivo verificar se uma família está apta para receber uma casa e executar as avaliações sobre o cadastro das famílias.

## Seleção
O módulo de seleção tem por objetivo filtrar as famílias que podem ser contempladas e ordenar as mesmas baseado na pontuação.


Para simular o funcionamento, no Main.java do projeto é feita uma seleção para 4 famílias e o resultado é impresso no console.

