package com.desafio.avaliacao;

import com.desafio.modelos.Dependente;
import com.desafio.modelos.Familia;
import com.desafio.modelos.Pessoa;

import java.util.List;

public class AvaliacaoDependentes implements Avaliacao{
    private boolean aprovado = false;
    public AvaliacaoDependentes() {
        super();
    }

    @Override
    public int calculaPontuacao(Familia familia) {
        List<Dependente> dependentes = familia.getDependentes();
        return calculaDependentesValidos(dependentes);
    }

    @Override
    public void setAprovado(boolean aprovado) {
        this.aprovado = aprovado;

    }

    @Override
    public boolean getAprovado() {
        return this.aprovado;
    }

    private int calculaDependentesValidos(List<Dependente> dependentes){
        int validos = 0;
        for (Pessoa dependente: dependentes){
            if(dependente.getIdade() < 18){
                validos++;
            }
        }
        if(validos != 0){
            this.setAprovado(true);
        }
        return validos;
    }

}
