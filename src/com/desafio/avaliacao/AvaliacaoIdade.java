package com.desafio.avaliacao;

import com.desafio.modelos.Familia;
import com.desafio.modelos.Pessoa;

public class AvaliacaoIdade implements Avaliacao{

    private boolean aprovado;

    @Override
    public int calculaPontuacao(Familia familia) {
        Pessoa pretendente = familia.getPretendente();
        this.setAprovado(true);
        int idade = pretendente.getIdade();
        if(idade >= 45){
            return 3;
        }else if(idade >= 30){
            return 2;
        }
        return 1;
    }

    @Override
    public void setAprovado(boolean aprovado) {
        this.aprovado = aprovado;
    }

    @Override
    public boolean getAprovado() {
        return this.aprovado;
    }
}
