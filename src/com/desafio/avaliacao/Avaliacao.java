package com.desafio.avaliacao;

import com.desafio.modelos.Familia;

public interface Avaliacao {
    
    int calculaPontuacao(Familia familia);

    void setAprovado(boolean b);

    boolean getAprovado();
}
