package com.desafio.avaliacao;

import com.desafio.modelos.Familia;

public class AvaliacaoRenda implements Avaliacao{

    private boolean aprovado;

    @Override
    public int calculaPontuacao(Familia familia) {
        float rendaTotal = familia.getRendaTotal();
        if (rendaTotal <= 900){
            return 5;
        }else if(rendaTotal <= 1500){
            return 3;
        }else if(rendaTotal <= 2000){
            return 1;
        }
        return 0;
    }

    @Override
    public void setAprovado(boolean aprovado) {
        this.aprovado = aprovado;
    }

    @Override
    public boolean getAprovado() {
        return this.aprovado;
    }
}
