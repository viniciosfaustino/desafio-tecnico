package com.desafio.validacao;

import com.desafio.modelos.Familia;

import static java.lang.Integer.parseInt;

public class ValidacaoFamilia {
    private Familia familia;
    private int status;
    private boolean apta;

    public ValidacaoFamilia(Familia familia) {
        this.familia = familia;
        this.defineStatus();
    }

    private void defineStatus(){
        int status = this.familia.getStatus();
        switch (status){
            case(1):
                this.setStatus(1);
                this.setApta(false);
                break;
            case(2):
                this.setStatus(2);
                this.setApta(true);
                break;
            case(3):
                this.setStatus(3);
                this.setApta(false);
                break;
            default:
                this.setStatus(0);
                this.setApta(false);
        }
    }

    private void setStatus(int status) {
        this.status = status;
    }

    public boolean isApta() {
        return apta;
    }

    public void setApta(boolean apta) {
        this.apta = apta;
    }
}
