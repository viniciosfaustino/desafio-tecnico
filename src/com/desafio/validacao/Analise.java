package com.desafio.validacao;

import com.desafio.avaliacao.Avaliacao;
import com.desafio.avaliacao.AvaliacaoDependentes;
import com.desafio.avaliacao.AvaliacaoIdade;
import com.desafio.avaliacao.AvaliacaoRenda;
import com.desafio.modelos.Familia;

import java.util.LinkedList;

public class Analise {
    private Familia familia;
    private int pontuacaoTotal;
    private int criteriosAtendidos;
    private LinkedList<Avaliacao> avaliacoes;


    public Analise(Familia familia) {
        this.familia = familia;
        initAvaliacoes();

    }

    private void initAvaliacoes(){
        this.avaliacoes = new LinkedList<Avaliacao>();
        AvaliacaoDependentes avaliacaoDependentes = new AvaliacaoDependentes();
        AvaliacaoIdade avaliacaoIdade = new AvaliacaoIdade();
        AvaliacaoRenda avaliacaoRenda = new AvaliacaoRenda();
        this.avaliacoes.add(avaliacaoDependentes);
        this.avaliacoes.add(avaliacaoIdade);
        this.avaliacoes.add(avaliacaoRenda);
    }

    public void analisarFamilia(){
        LinkedList<Avaliacao> avaliacoes = this.getAvaliacoes();
        Familia familia = this.getFamilia();
        int pontuacao = 0;
        for(Avaliacao avaliacao: avaliacoes){
            pontuacao += avaliacao.calculaPontuacao(familia);
        }
        this.setPontuacaoTotal(pontuacao);
        this.contaCriteriosAtendidos();
    }

    public void contaCriteriosAtendidos(){
        LinkedList<Avaliacao> avaliacoes = this.getAvaliacoes();
        int criteriosAtendidos = 0;
        for(Avaliacao avaliacao: avaliacoes){
            if(avaliacao.getAprovado()){
                criteriosAtendidos++;
            }
        }
        this.setCriteriosAtendidos(criteriosAtendidos);
    }

    public LinkedList<Avaliacao> getAvaliacoes() {
        return this.avaliacoes;
    }

    public Familia getFamilia() {
        return familia;
    }

    public int getPontuacaoTotal() {
        return pontuacaoTotal;
    }

    public int getCriteriosAtendidos() {
        return criteriosAtendidos;
    }

    public void setCriteriosAtendidos(int criteriosAtendidos) {
        this.criteriosAtendidos = criteriosAtendidos;
    }

    public void setPontuacaoTotal(int pontuacaoTotal) {
        this.pontuacaoTotal = pontuacaoTotal;
    }
}
