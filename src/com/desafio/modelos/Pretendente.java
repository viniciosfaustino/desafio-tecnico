package com.desafio.modelos;

import java.util.Calendar;

public class Pretendente extends Pessoa{
    public Pretendente(Calendar dataDeNascimento, String id, String nome, String tipo) {
        super(dataDeNascimento, id, nome, tipo);
    }

    public Pretendente() {
    }
}
