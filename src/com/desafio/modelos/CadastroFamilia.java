package com.desafio.modelos;

import java.util.Calendar;

public class CadastroFamilia implements Comparable<CadastroFamilia>{
    private Familia familia;
    private Calendar dataSelecao;
    private int pontuacaoTotal;
    private int quantidadeDeCriteriosAtendidos;

    public CadastroFamilia(Familia familia) {
        this.familia = familia;

    }

    public CadastroFamilia(Familia familia, Calendar dataSelecao, int pontuacaoTotal, int quantidadeDeCriteriosAtendidos) {
        this.familia = familia;
        this.dataSelecao = dataSelecao;
        this.pontuacaoTotal = pontuacaoTotal;
        this.quantidadeDeCriteriosAtendidos = quantidadeDeCriteriosAtendidos;
    }

    public Familia getFamilia() {
        return familia;
    }

    public void setFamilia(Familia familia) {
        this.familia = familia;
    }

    public Calendar getDataSelecao() {
        return dataSelecao;
    }

    public void setDataSelecao(Calendar dataSelecao) {
        this.dataSelecao = dataSelecao;
    }

    public int getPontuacaoTotal() {
        return pontuacaoTotal;
    }

    public void setPontuacaoTotal(int pontuacaoTotal) {
        this.pontuacaoTotal = pontuacaoTotal;
    }

    public int getQuantidadeDeCriteriosAtendidos() {
        return quantidadeDeCriteriosAtendidos;
    }

    public void setQuantidadeDeCriteriosAtendidos(int quantidadeDeCriteriosAtendidos) {
        this.quantidadeDeCriteriosAtendidos = quantidadeDeCriteriosAtendidos;
    }


    @Override
    public int compareTo(CadastroFamilia c) {
        if (this.getPontuacaoTotal() > c.getPontuacaoTotal()){
            return 1;
        }
        else if(this.getPontuacaoTotal() < c.getPontuacaoTotal()){
            return -1;
        }
        return 0;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this){
            return true;
        }

        if (obj == null || obj.getClass() != this.getClass()){
            return false;
        }

        CadastroFamilia cadastro = (CadastroFamilia) obj;
        return cadastro.getFamilia() == this.getFamilia();
    }

    @Override
    public String toString() {
        return "CadastroFamilia{" +
                "familia=" + familia.getId() +
                ", dataSelecao=" + dataSelecao.getTime() +
                ", pontuacaoTotal=" + pontuacaoTotal +
                ", quantidadeDeCriteriosAtendidos=" + quantidadeDeCriteriosAtendidos +
                '}';
    }
}
