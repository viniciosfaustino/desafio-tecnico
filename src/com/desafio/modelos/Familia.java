package com.desafio.modelos;

import java.util.LinkedList;

public class Familia {
    private String id;
    private LinkedList<Pessoa> pessoas;
    private LinkedList<Renda> rendas;
    private Pretendente pretendente;
    private Conjuge conjuge;
    private LinkedList<Dependente> dependentes;
    private int status;
    private float rendaTotal;

    public Familia(String id, LinkedList<Renda> rendas, Pretendente pretendente, Conjuge conjuge, LinkedList<Dependente> dependentes,
                   int status) {
        this.id = id;
        this.rendas = rendas;
        this.pretendente = pretendente;
        this.conjuge = conjuge;
        this.dependentes = dependentes;
        this.status = status;
        this.pessoas = new LinkedList<Pessoa>();
        this.adicionaPessoas();
        this.setRendaTotal(this.calculaRendaTotal());
    }

    public Familia() {
    }

    private void adicionaPessoas(){
        this.pessoas.add(this.pretendente);
        this.pessoas.add(this.conjuge);
        for(Pessoa dependente: this.dependentes){
            this.pessoas.add(dependente);
        }
    }

    private void adicionaPessoas(Pessoa pretendente, Pessoa conjuge, LinkedList<Pessoa> dependentes){
        this.pessoas.add(pretendente);
        this.pessoas.add(conjuge);
        for(Pessoa dependente: dependentes){
            this.pessoas.add(dependente);
        }
    }

    public String getId() {
        return id;
    }

    public LinkedList<Pessoa> getPessoas() {
        return pessoas;
    }

    public LinkedList<Renda> getRendas() {
        return rendas;
    }

    public int getStatus() {
        return status;
    }

    public Pessoa getPretendente() {
        return pretendente;
    }

    public Pessoa getConjuge() {
        return conjuge;
    }

    public LinkedList<Dependente> getDependentes() {
        return dependentes;
    }

    public Pessoa getPessoaById(String id){
        for(Pessoa pessoa: this.getPessoas()){
            if (pessoa.getId().equals(id)){
                return pessoa;
            }
        }
        return null;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setPessoas(LinkedList<Pessoa> pessoas) {
        this.pessoas = pessoas;
    }

    public void setRendas(LinkedList<Renda> rendas) {
        this.rendas = rendas;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public void setPretendente(Pretendente pretendente) {
        this.pretendente = pretendente;
    }

    public void setConjuge(Conjuge conjuge) {
        this.conjuge = conjuge;
    }

    public void setDependentes(LinkedList<Dependente> dependentes) {
        this.dependentes = dependentes;
    }

    public void setRendaTotal(float rendaTotal) {
        this.rendaTotal = rendaTotal;
    }

    public float getRendaTotal() {
        return rendaTotal;
    }

    public float calculaRendaTotal(){
        float rendaTotal = 0f;
        for (Renda renda: this.rendas){
            rendaTotal += renda.getValor();
        }
        return rendaTotal;
    }

    @Override
    public String toString() {
        return "Familia{" +
                "id='" + id + '\'' +
                ", pessoas=" + pessoas +
                ", rendas=" + rendas +
                ", pretendente=" + pretendente +
                ", conjuge=" + conjuge +
                ", dependentes=" + dependentes +
                ", status='" + status + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object obj) {
        Familia familia = (Familia) obj;
        return this.getId().equals(familia.getId());
    }
}
