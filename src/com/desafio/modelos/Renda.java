package com.desafio.modelos;

public class Renda {
    private String pessoaId;
    private float valor;

    public Renda(String pessoaId, float valor) {
        this.pessoaId = pessoaId;
        this.valor = valor;
    }

    public String getPessoaId() {
        return pessoaId;
    }

    public void setPessoa(String id) {
        this.pessoaId = id;
    }

    public float getValor() {
        return valor;
    }

    public void setValor(float valor) {
        this.valor = valor;
    }

    @Override
    public String toString() {
        return "Renda{" +
                "pessoaId='" + pessoaId + '\'' +
                ", valor=" + valor +
                '}';
    }
}
