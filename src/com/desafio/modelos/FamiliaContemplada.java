package com.desafio.modelos;

import java.util.Calendar;

public class FamiliaContemplada {
    private String id;
    private int quantidadeCriteriosAtendidos;
    private int pontuacaoTotal;
    private Calendar dataSelecao;

    public FamiliaContemplada(CadastroFamilia cadastro){
        this.id = cadastro.getFamilia().getId();
        this.quantidadeCriteriosAtendidos = cadastro.getQuantidadeDeCriteriosAtendidos();
        this.pontuacaoTotal = cadastro.getPontuacaoTotal();
        this.dataSelecao = cadastro.getDataSelecao();
    }

    public String getId() {
        return id;
    }

    public int getQuantidadeCriteriosAtendidos() {
        return quantidadeCriteriosAtendidos;
    }

    public int getPontuacaoTotal() {
        return pontuacaoTotal;
    }

    public Calendar getDataSelecao() {
        return dataSelecao;
    }

    @Override
    public String toString() {
        return "FamiliaContemplada{" +
                "id='" + id + '\'' +
                ", quantidadeCriteriosAtendidos=" + quantidadeCriteriosAtendidos +
                ", pontuacaoTotal=" + pontuacaoTotal +
                ", dataSelecao=" + dataSelecao.getTime() +
                '}';
    }
}
