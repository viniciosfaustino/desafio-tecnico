package com.desafio.modelos;

import java.util.Calendar;

public class Dependente extends Pessoa{
    public Dependente(Calendar dataDeNascimento, String id, String nome, String tipo) {
        super(dataDeNascimento, id, nome, tipo);
    }

    public Dependente() {
    }
}
