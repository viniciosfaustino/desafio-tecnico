package com.desafio.modelos;

import java.time.LocalDate;
import java.time.Period;
import java.util.Calendar;

public class Pessoa {
    private Calendar dataDeNascimento;
    private String id;
    private String nome;
    private String tipo;

    public Pessoa(Calendar dataDeNascimento, String id, String nome, String tipo) {
        this.dataDeNascimento = dataDeNascimento;
        this.id = id;
        this.nome = nome;
        this.tipo = tipo;
    }

    public Pessoa() {
    }

    public Calendar getDataDeNascimento() {
        return dataDeNascimento;
    }

    public void setDataDeNascimento(Calendar dataDeNascimento) {
        this.dataDeNascimento = dataDeNascimento;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public int getIdade(){
        Calendar c = Calendar.getInstance();
        int ano = c.get(Calendar.YEAR);
        int mes = c.get(Calendar.MONTH) + 1;
        int data = c.get(Calendar.DATE);
        LocalDate l = LocalDate.of(ano, mes, data);
        LocalDate hoje = LocalDate.now();
        Period diferenca = Period.between(l, hoje);
        return diferenca.getYears();
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    @Override
    public String toString() {
        return "Pessoa{" +
//                "dataDeNascimento=" + dataDeNascimento +
                ", id='" + id + '\'' +
                ", nome='" + nome + '\'' +
                ", tipo='" + tipo + '\'' +
                '}';
    }
}


