package com.desafio;

import com.desafio.dados.CarregaDados;
import com.desafio.modelos.CadastroFamilia;
import com.desafio.modelos.Familia;
import com.desafio.modelos.FamiliaContemplada;
import com.desafio.selecao.Selecao;
import com.desafio.validacao.Analise;

import java.text.ParseException;
import java.util.Calendar;
import java.util.LinkedList;

public class Main {

    public static void main(String[] args) throws ParseException {
        CarregaDados dados = new CarregaDados();
        LinkedList<Familia> familias = dados.carregaFamilias();
        LinkedList<CadastroFamilia> cadastrosFamilias = new LinkedList<>();
        Selecao selecao = new Selecao();
        Calendar c = Calendar.getInstance();
        CadastroFamilia cadastro;
        Analise analise;
        for(Familia f: familias){
            analise = new Analise(f);
            analise.analisarFamilia();
            cadastro = new CadastroFamilia(f, c, analise.getPontuacaoTotal(), analise.getCriteriosAtendidos());
            cadastrosFamilias.add(cadastro);
        }
        selecao.adicionaFamiliasCadastradas(cadastrosFamilias);
        selecao.selecionaFamilias();
        LinkedList<FamiliaContemplada> top =selecao.devolveFamiliasContempladas();
        System.out.println(top);

    }

}
