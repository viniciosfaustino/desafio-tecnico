package com.desafio.dados;
import com.desafio.modelos.*;
import org.json.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.LinkedList;

public class CarregaDados {
    public final static String dados(){
//        return ("{\"familia\":{"
//                +  "\"id\": \"3dac7da3-d742-4e51-95f9-bbb37f522413\","
//                +  "\"pessoas\": ["
//                +    "{ \"id\": \"5e65eea1-aa72-407e-9a67-88045c07b5de\", \"nome\": \"João\", \"tipo\": \"Pretendente\", \"dataDeNascimento\": \"1989-12-30\" },"
//                +    "{ \"id\": \"d467781a-8f06-45ba-be6f-879cf32a9f7e\", \"nome\": \"Maria\", \"tipo\": \"Cônjuge\", \"dataDeNascimento\": \"1989-12-30\" },"
//                +    "{ \"id\": \"79820382-a181-42d2-bfae-6c012489e65e\", \"nome\": \"José\", \"tipo\": \"Dependente\", \"dataDeNascimento\": \"2015-12-30\" },"
//                +    "{ \"id\": \"80fa071e-17fb-4b87-99db-a7db0bfc23c2\", \"nome\": \"Angela\", \"tipo\": \"Dependente\", \"dataDeNascimento\": \"2015-12-30\" }"
//                +  "],"
//                +  "\"rendas\": ["
//                +    "{ \"pessoaId\": \"5e65eea1-aa72-407e-9a67-88045c07b5de\", \"valor\": 1000 },"
//                +    "{ \"pessoaId\": \"d467781a-8f06-45ba-be6f-879cf32a9f7e\", \"valor\": 950 }"
//                +  "],"
//                +  "\"status\": \"2\""
//                +"}}");

        return(          "{\"familias\":[{"
                        +"  \"id\": \"c951ae3616378699d284d7d87e7e24e1d88be778\","
                        +"  \"pessoas\": ["
                        +"    {"
                        +"      \"id\": \"5195764da76ad681cf99c672fedae53af04289d8\","
                        +"      \"nome\": \"Thiago\","
                        +"      \"tipo\": \"Pretendente\","
                        +"      \"dataDeNascimento\": \"1980-10-17\""
                        +"    },"
                        +"    {"
                        +"      \"id\": \"2127e25a68ac1279e65e28d28227f8431bba9e64\","
                        +"      \"nome\": \"Enzo\","
                        +"      \"tipo\": \"Cônjuge\","
                        +"      \"dataDeNascimento\": \"1968-4-27\""
                        +"    },"
                        +"    {"
                        +"      \"id\": \"83ae2d56482ff4968fc62cc0e58e77dbe78c33e9\","
                        +"      \"nome\": \"João\","
                        +"      \"tipo\": \"Dependente\","
                        +"      \"dataDeNascimento\": \"2007-3-14\""
                        +"    },"
                        +"    {"
                        +"      \"id\": \"e0a8a679438b42c7fbabc247282323a1fbfdd314\","
                        +"      \"nome\": \"Gabriel\","
                        +"      \"tipo\": \"Dependente\","
                        +"      \"dataDeNascimento\": \"2017-2-12\""
                        +"    },"
                        +"    {"
                        +"      \"id\": \"dc7daaeaddc28476e8a41c4a893a214d52c7e3dc\","
                        +"      \"nome\": \"Heitor\","
                        +"      \"tipo\": \"Dependente\","
                        +"      \"dataDeNascimento\": \"1999-2-2\""
                        +"    },"
                        +"    {"
                        +"      \"id\": \"3a4d4b972abf2eb1d085f95c39f158569343a2b9\","
                        +"      \"nome\": \"Bernardo\","
                        +"      \"tipo\": \"Dependente\","
                        +"      \"dataDeNascimento\": \"2010-8-28\""
                        +"    }"
                        +"  ],"
                        +"  \"rendas\": ["
                        +"    { \"pessoaId\": \"5195764da76ad681cf99c672fedae53af04289d8\", \"valor\": 502 },"
                        +"    { \"pessoaId\": \"2127e25a68ac1279e65e28d28227f8431bba9e64\", \"valor\": 968 }"
                        +"  ],"
                        +"  \"status\": 2"
                        +"},"
                        +"{"
                        +"  \"id\": \"3787042ceb05ffb9c6abdd55d11a5d8ca84800ff\","
                        +"  \"pessoas\": ["
                        +"    {"
                        +"      \"id\": \"941f5dd0979188186a11d13d1fc130fb66b7951b\","
                        +"      \"nome\": \"Ruan\","
                        +"      \"tipo\": \"Pretendente\","
                        +"      \"dataDeNascimento\": \"1994-6-19\""
                        +"    },"
                        +"    {"
                        +"      \"id\": \"7c70476f5b01b8fdae86bd2bad3b818b51d96072\","
                        +"      \"nome\": \"Pedro\","
                        +"      \"tipo\": \"Cônjuge\","
                        +"      \"dataDeNascimento\": \"1991-6-13\""
                        +"    }"
                        +"  ],"
                        +"  \"rendas\": ["
                        +"    { \"pessoaId\": \"941f5dd0979188186a11d13d1fc130fb66b7951b\", \"valor\": 502 },"
                        +"    { \"pessoaId\": \"7c70476f5b01b8fdae86bd2bad3b818b51d96072\", \"valor\": 1294 }"
                        +"  ],"
                        +"  \"status\": 2"
                        +"},"
                        +"{"
                        +"  \"id\": \"5d177d4ad6194655434eff71ac7b2c80a939073c\","
                        +"  \"pessoas\": ["
                        +"    {"
                        +"      \"id\": \"527adf1fe9271a47e6e96275df0a5bc7dbf694f6\","
                        +"      \"nome\": \"Bryan\","
                        +"      \"tipo\": \"Pretendente\","
                        +"      \"dataDeNascimento\": \"1990-11-11\""
                        +"    },"
                        +"    {"
                        +"      \"id\": \"b36a012aca2d22dec6aa749aad18e6ccbe4c88db\","
                        +"      \"nome\": \"Rodrigo\","
                        +"      \"tipo\": \"Cônjuge\","
                        +"      \"dataDeNascimento\": \"1990-12-21\""
                        +"    }"
                        +"  ],"
                        +"  \"rendas\": ["
                        +"    { \"pessoaId\": \"527adf1fe9271a47e6e96275df0a5bc7dbf694f6\", \"valor\": 1446 },"
                        +"    { \"pessoaId\": \"b36a012aca2d22dec6aa749aad18e6ccbe4c88db\", \"valor\": 943 }"
                        +"  ],"
                        +"  \"status\": 2"
                        +"},"
                        +"{"
                        +"  \"id\": \"83f1d78490d7cc4b09151840b9669c0fa1cb7f04\","
                        +"  \"pessoas\": ["
                        +"    {"
                        +"      \"id\": \"4c26b30ce0050803d92745b3fc1e68d9f8a67030\","
                        +"      \"nome\": \"Bernardo\","
                        +"      \"tipo\": \"Pretendente\","
                        +"      \"dataDeNascimento\": \"1983-2-2\""
                        +"    },"
                        +"    {"
                        +"      \"id\": \"1b207f5af8566a79a75ce40d1c1039f41941b00a\","
                        +"      \"nome\": \"Davi\","
                        +"      \"tipo\": \"Cônjuge\","
                        +"      \"dataDeNascimento\": \"1989-2-3\""
                        +"    },"
                        +"    {"
                        +"      \"id\": \"c32bf4e7a7ddc685c888d45d2e91288f9a23ce5a\","
                        +"      \"nome\": \"Liz\","
                        +"      \"tipo\": \"Dependente\","
                        +"      \"dataDeNascimento\": \"2006-10-4\""
                        +"    },"
                        +"    {"
                        +"      \"id\": \"c4aa435e6501861b9360ee3ded43fb794d6e8495\","
                        +"      \"nome\": \"Laura\","
                        +"      \"tipo\": \"Dependente\","
                        +"      \"dataDeNascimento\": \"2001-1-2\""
                        +"    }"
                        +"  ],"
                        +"  \"rendas\": ["
                        +"    { \"pessoaId\": \"4c26b30ce0050803d92745b3fc1e68d9f8a67030\", \"valor\": 621 },"
                        +"    { \"pessoaId\": \"1b207f5af8566a79a75ce40d1c1039f41941b00a\", \"valor\": 1008 }"
                        +"  ],"
                        +"  \"status\": 2"
                        +"}]}"
                );
    }

    private LinkedList<Renda> carregaRendas(JSONArray rendasJson){
        JSONObject tmp = new JSONObject();
        float valor;
        String pessoaId;
        LinkedList<Renda> rendas = new LinkedList<>();
        for (int i = 0; i < rendasJson.length(); i++) {
            tmp = (JSONObject) rendasJson.get(i);
            valor = tmp.getFloat("valor");
            pessoaId = tmp.getString("pessoaId");
            rendas.add(new Renda(pessoaId, valor));
        }
        return rendas;
    }

    public LinkedList<Familia> carregaFamilias() throws ParseException {
        LinkedList<Familia> familias = new LinkedList<>();
        String data = dados();
        final JSONObject obj = new JSONObject(data);
        JSONArray familiasJson = obj.getJSONArray("familias");

        JSONObject familiaJson = new JSONObject();
        for (int j = 0; j < familiasJson.length(); j++) {
            familiaJson = (JSONObject) familiasJson.get(j);
            JSONArray rendasJson = familiaJson.getJSONArray("rendas");
            JSONArray pessoasJson = familiaJson.getJSONArray("pessoas");
            String id = familiaJson.getString("id");
            LinkedList<Renda> rendas = new LinkedList<>();
            rendas = carregaRendas(rendasJson);
            String nome;
            String tipo;
            String dataDeNascimento;
            Pretendente pretendente = new Pretendente();
            Conjuge conjuge = new Conjuge();
            LinkedList<Dependente> dependentes = new LinkedList<>();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Calendar c = Calendar.getInstance();
            String pessoaId;
            JSONObject tmp;
            for (int i = 0; i < pessoasJson.length(); i++) {
                tmp = (JSONObject) pessoasJson.get(i);
                pessoaId = tmp.getString("id");
                tipo = tmp.getString("tipo");
                nome = tmp.getString("nome");
                dataDeNascimento = tmp.getString("dataDeNascimento");
                c.setTime(sdf.parse(dataDeNascimento));
                if (tipo.equals("Pretendente")) {
                    pretendente = new Pretendente(c, pessoaId, nome, tipo);
                } else if (tipo.equals("Cônjuge")) {
                    conjuge = new Conjuge(c, pessoaId, nome, tipo);
                } else {
                    dependentes.add(new Dependente(c, pessoaId, nome, tipo));
                }
            }

            int status;
            status = familiaJson.getInt("status");

            Familia familia = new Familia(id, rendas, pretendente, conjuge, dependentes, status);
            familias.add(familia);
        }
        return familias;
    }
}
