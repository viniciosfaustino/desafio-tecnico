package com.desafio.selecao;

import com.desafio.modelos.CadastroFamilia;
import com.desafio.modelos.FamiliaContemplada;
import com.desafio.validacao.ValidacaoFamilia;

import java.util.Comparator;
import java.util.LinkedList;
import java.util.stream.Collectors;

public class Selecao {
    private LinkedList<CadastroFamilia> familiasCadastradas;
    private LinkedList<CadastroFamilia> familiasSelecionadas;

    public Selecao() {
        this.familiasCadastradas = new LinkedList<>();
        this.familiasSelecionadas = new LinkedList<>();
    }

    public LinkedList<CadastroFamilia> getFamiliasCadastradas() {
        return familiasCadastradas;
    }

    public void setFamiliasCadastradas(LinkedList<CadastroFamilia> familiasCadastradas) {
        this.familiasCadastradas = familiasCadastradas;
    }

    public LinkedList<CadastroFamilia> getFamiliasSelecionadas() {
        return familiasSelecionadas;
    }

    public LinkedList<CadastroFamilia> getFamiliasSelecionadas(int quantidadeMaxima) {
        return familiasSelecionadas.stream().limit(quantidadeMaxima).collect(Collectors.toCollection(LinkedList::new));
    }


    public void adicionaFamiliasSelecionadas(LinkedList<CadastroFamilia> cadastros){
        LinkedList<CadastroFamilia> familiasSelecionadas = this.getFamiliasSelecionadas();
        for(CadastroFamilia cadastro: cadastros){
            if(!familiasSelecionadas.contains(cadastro)){
                this.adicionaFamiliaSelecionada(cadastro);
            }
        }
        this.ordenaFamiliasSelecionadas();
    }

    private void adicionaFamiliaSelecionada(CadastroFamilia cadastro){
        this.familiasSelecionadas.add(cadastro);
        this.ordenaFamiliasSelecionadas();
    }

    public void adicionaFamiliasCadastradas(LinkedList<CadastroFamilia> familias){
        for(CadastroFamilia cadastro: familias){
            if(!familiasSelecionadas.contains(cadastro)){
                this.adicionaFamiliaCadastrada(cadastro);
            }
        }
    }

    public void adicionaFamiliaCadastrada(CadastroFamilia cadastro){
        if(!this.getFamiliasCadastradas().contains(cadastro)){
            this.familiasCadastradas.add(cadastro);
        }
    }

    private void ordenaFamiliasSelecionadas(){
        this.getFamiliasSelecionadas().sort(Comparator.reverseOrder());
    }

    public void selecionaFamilias(){
        ValidacaoFamilia validacao;
        for(CadastroFamilia cadastro: this.getFamiliasCadastradas()){
            validacao = new ValidacaoFamilia(cadastro.getFamilia());
            if (validacao.isApta()){
                this.adicionaFamiliaSelecionada(cadastro);
            }
        }
    }

    public LinkedList<FamiliaContemplada> devolveFamiliasContempladas(){
        LinkedList<FamiliaContemplada> contemplados = new LinkedList<>();
        for(CadastroFamilia cadastro: this.familiasSelecionadas){
            contemplados.add(new FamiliaContemplada(cadastro));
        }
        return contemplados;
    }

}
